/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function basicInfo(){
	alert("Please input your details.");
	let fullName = prompt("Enter your full name: "); 
	let age = prompt("Enter your age: "); 
	let location = prompt("Enter your location :");

	console.log("Hello, "+fullName+"."); 
	console.log("You are "+age+" years old."); 
	console.log("You live in "+location+"."); 
	}

	basicInfo();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favBandArtist() {
		alert("Please input your top 5 bands/musical artist details.");
		let band1 = prompt("Please enter your top 1 band/musical artist. ");
		let band2 = prompt("Please enter your top 2 band/musical artist. ");
		let band3 = prompt("Please enter your top 3 band/musical artist. ");
		let band4 = prompt("Please enter your top 4 band/musical artist. ");
		let band5 = prompt("Please enter your top 5 band/musical artist. ");

		
		console.log("1. "+band1);
		console.log("2. "+band2);
		console.log("3. "+band3);
		console.log("4. "+band4);
		console.log("5. "+band5);


	}

	favBandArtist();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favMovies() {
		let movieTop1 = "The Pursuit of Happyness \nRotten Tomatoes Rating: 67%";
		let movieTop2 = "Forest Gump \nRotten Tomatoes Rating: 71%";
		let movieTop3 = "Cast Away \nRotten Tomatoes Rating: 89%";
		let movieTop4 = "The Godfather \nRotten Tomatoes Rating: 97%";
		let movieTop5 = "The Godfather part II \nRotten Tomatoes Rating: 96%";

		alert("Top 5 Movies\nTop 1: "+movieTop1+"\nTop 2: "+movieTop2+"\nTop 3: "+movieTop3+"\nTop 4: "+movieTop4+"\nTop 5: "+movieTop5);
		console.log("1: "+movieTop1);
		console.log("2: "+movieTop2);
		console.log("3: "+movieTop3);
		console.log("4: "+movieTop4);
		console.log("5: "+movieTop5);
	}

	favMovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();

// let printFriends = 

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

// console.log(friend1);
// console.log(friend2);